<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Http\Controllers\StateController;

class UnitTesting extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testStateResults()
    {
        $controller = new StateController();

        $response = $controller->getResult("S0");
        $this->assertEquals(0,$response);

        $response = $controller->getResult("S1");
        $this->assertEquals(1,$response);

        $response = $controller->getResult("S2");
        $this->assertEquals(2,$response);

        $response = $controller->getResult("S3");
        $this->assertEquals("Invalid State",$response);

    }

    public function testTransition()
    {
        $controller = new StateController();
        
        $response = $controller->transition("S0","0");
        $this->assertEquals("S0",$response);

        $response = $controller->transition("S0","1");
        $this->assertEquals("S1",$response);

        $response = $controller->transition("S1","0");
        $this->assertEquals("S2",$response);

        $response = $controller->transition("S1","1");
        $this->assertEquals("S0",$response);

        $response = $controller->transition("S2","0");
        $this->assertEquals("S1",$response);

        $response = $controller->transition("S2","1");
        $this->assertEquals("S2",$response);

        $response = $controller->transition("S3","0");
        $this->assertEquals("Invalid State",$response);

        $response = $controller->transition("S0","s");
        $this->assertEquals("Invalid Input",$response);

    }

    public function testFinalResult()
    {
        $controller = new StateController();
        
        $response = $controller->getFinalResult("110");
        
        $this->assertEquals(
            (object) [
                "status" => "success",
                "beginState" => [
                    "S0",
                    "S1",
                    "S0"
                ],
                "input" => [
                    "1",
                    "1",
                    "0"
                ],
                "endState" => [
                    "S1",
                    "S0",
                    "S0"
                ],
                "result" => "0"
            ], $response
        );

        $response = $controller->getFinalResult("1010");
        
        $this->assertEquals(
            (object) [
                "status" => "success",
                "beginState" => [
                    "S0",
                    "S1",
                    "S2",
                    "S2"
                ],
                "input" => [
                    "1",
                    "0",
                    "1",
                    "0"
                ],
                "endState" => [
                    "S1",
                    "S2",
                    "S2",
                    "S1"
                ],
                "result" => "1"
            ], $response
        );

        $response = $controller->getFinalResult("testing");
        
        $this->assertEquals(
            (object) [
                "status" => "failed",
                "result" => "Invalid Input"
            ], $response
        );

    }
}
