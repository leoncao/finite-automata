<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Finite State Automata</title>

        {{-- Fonts --}}
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        {{-- Compiled CSS --}}
        <link href="{{ asset('css/compiled/app.css')}}" rel="stylesheet">

        {{-- Styles --}}
        <link href="{{ asset('css/custom.css')}}" rel="stylesheet">

    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title">
                    Finite State Automata
                </div>

                <div class="heading">
                    <p>This automata will gives you the remainder when dividing a number (in binary) by 3</p>
                </div>

                <div class="form">
                    <p>Please enter a string of binaries (0's and 1's)</p>
                    <form method="POST">
                        {{ csrf_field() }}
                        <input class="form-control" name="input" autofocus required>
                        <button class="btn btn-primary mt-3" type="submit">Submit</button>
                    </form>
                </div>

                @if(isset($results))
                    <div class="results mt-3">
                        <div class="jumbotron">
                            <h1>Result</h1>
                            <hr>
                        <p>Initial State: S0 | Original Input: {{ $originalInput }}</p> 
                            @if($results->status == "success")
                                @foreach($results->beginState as $i=>$state)
                                <p>{{$state}} with input: {{$results->input[$i]}} => {{$results->endState[$i]}}</p>
                                @endforeach
                                <p id="answer"> Answer: {{ $results->result }} </p>
                            @elseif($results->status == "failed")
                                <p id="answer"> {{ $results->result }} </p>
                            @else
                                <p id="answer"> An error has occurred </p>
                            @endif
                            
                        </div>
                    </div>

                @endif
            </div>
        </div>
    </body>
</html>
