<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StateController extends Controller
{

    public function index() {
        return view('index');
    }

    public function results(Request $request) {

        $originalInput = $request->input;
        
        $results = $this->getFinalResult($originalInput);

        return view('index',compact('results','originalInput'));
    }

    public function getFinalResult($originalInput) {
        $stateArray = array();
        $inputArray = array();
        $resultArray = array();

        $currentState = "S0";

        $inputString = str_split($originalInput);

        foreach($inputString as $input){
            // This will save the begining state for this input
            array_push($stateArray,$currentState);
            // This will save the input for this index
            array_push($inputArray,$input);

            $currentState = $this->transition($currentState,$input);

            // if failed return fail statement
            if($currentState == "Invalid Input"){
                return (object) array("status" => "failed","result" => "Invalid Input");
            }

            // This will save the result for this input
            array_push($resultArray,$currentState);
            
        }

        // This will get the final result
        $finalResult = $this->getResult($currentState);

        // Save into results obj
        return (object) array(
            "status" => "success",
            "beginState" => $stateArray, 
            "input" => $inputArray, 
            "endState" => $resultArray, 
            "result" => $finalResult
        );
    }

    public function transition($state,$input) {
        if($state == "S0"){
            if($input == "0"){
                return "S0";
            }elseif($input == "1"){
                return "S1";
            }else{
                return "Invalid Input";
            }
        }elseif($state == "S1"){
            if($input == "0"){
                return "S2";
            }elseif($input == "1"){
                return  "S0";
            }else{
                return "Invalid Input";
            }
        }elseif($state == "S2"){
            if($input == "0"){
                return "S1";
            }elseif($input == "1"){
                return "S2";
            }else{
                return "Invalid Input";
            }
        }else{
            return "Invalid State";
        }
    }

    public function getResult($state) {
        if($state == "S0"){
            return "0";
        }elseif($state == "S1"){
            return "1";
        }elseif($state == "S2"){
            return "2";
        }else{
            return "Invalid State";
        }
    }
}
